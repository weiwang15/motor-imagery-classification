Due to the large size of data, the raw data is not part of the repository.
Download the data and place it in this folder as described in the pdf in this folder.
The data can be downloaded at http://bnci-horizon-2020.eu/database/data-sets