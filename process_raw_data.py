from os import scandir
from session_data import get_session_data_from_file
from session_data import SessionData


def process_session_signals(data: SessionData) -> object:
    """
    process data according to the paper: split it into chunks, perform signal processing stuff

    :param data: data to be processed
    :return: processed data
    """
    session_signals = data.signals
    for trial_idx in range(0, session_signals.shape[0]):
        for channel_idx in range(0, session_signals.shape[1]):
            trial_channel_signal = session_signals[trial_idx, channel_idx, :]
            # TODO:
            #  - do signal processing on trial_channel_signal.
            #  - store the processed signal values.
            #  - return it all.


def store_processed_data(processed_data_directory: str, processed_data: object) -> None:
    """
    stores processed data

    :param processed_data_directory: directory where data will be stored
    :param processed_data: data to be stored
    """
    # TODO (4) store data. Maybe as pickle or something.
    pass


def print_some_data(sd: SessionData):
    print(f"num={sd.subject_number},\t"
          f"type={sd.session_type},\t"
          f"#eog={sd.num_eog_runs},\t"
          f"signal_shape:{sd.signals.shape},\t"
          f"label_shape:{sd.labels.shape}")


def main():
    """
    Goes through all raw data files, processes them and stores them in another folder
    """

    raw_data_directory = r'./raw_data'
    processed_data_directory = r'./processed_data'
    for entry in scandir(raw_data_directory):
        file_path = entry.path
        if file_path[-3:] == 'mat':
            session_data = get_session_data_from_file(file_path)
            # print_some_data(session_data)
            processed_data = process_session_signals(session_data)

        # processed_data = process_data(data, labels)
        # store_processed_data(processed_data_directory, processed_data)


if __name__ == '__main__':
    main()
