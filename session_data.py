"""This file contains:
- SessionData dataclass definition
- get_session_data to extract a SessionData object from a file
- some constants relevant for data extraction
"""

import numpy as np
import scipy.io as sio
from dataclasses import dataclass

"""
Constants:
"""
num_channels = 22

num_runs = 6
num_trials_per_run = 48
num_trials = num_runs * num_trials_per_run  # 288

seconds_per_window = 7
samples_per_second = 250
window_length = seconds_per_window * samples_per_second  # 1750


@dataclass
class SessionData:
    subject_number: int
    session_type: str
    num_eog_runs: int
    signals: np.ndarray
    labels: np.ndarray


def get_session_data_from_file(file_path: str) -> SessionData:
    """
    Loads the dataset 2a of the BCI Competition IV into SessionData object

    Return:	SessionData object
    """
    subject_number = int(file_path[-6])
    if file_path[-5] == 'T':
        session_type = "Training"
    else:
        session_type = "Evaluation"

    if subject_number == 4 and session_type == "Training":
        num_eog_runs = 1
    else:
        num_eog_runs = 3

    session_labels = np.zeros(num_trials)
    session_signals = np.zeros((num_trials, num_channels, window_length))

    valid_trial_idx = 0
    data = sio.loadmat(file_path)['data']
    for run_idx in range(0, data.size):
        run_data = [data[0, run_idx][0, 0]][0]  # some silly index magic
        run_signal = run_data[0]
        run_trials = run_data[1]
        run_labels = run_data[2]
        run_artifacts = run_data[5]
        for trial_idx in range(0, run_trials.size):
            num_artifacts_in_this_trial = run_artifacts[trial_idx]
            if num_artifacts_in_this_trial == 0:
                session_signals[valid_trial_idx, :, :] = np.transpose(
                    run_signal[int(run_trials[trial_idx]):(int(run_trials[trial_idx]) + window_length), :num_channels])
                session_labels[valid_trial_idx] = int(run_labels[trial_idx])
                valid_trial_idx += 1

    session_data = SessionData(subject_number,
                               session_type,
                               num_eog_runs,
                               signals=session_signals[0:valid_trial_idx, :, :],
                               labels=session_labels[0:valid_trial_idx])

    return session_data
